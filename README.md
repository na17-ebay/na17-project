**Projet NA17 A19 PLATEFORME D’ECHANGE A L’UTC**

Groupe de projet numéro 2 : DÉFOSSEZ Juliette - HURBLAIN Loïc - JATYKA Julien - JOUBIN Hippolyte - LACOUR Anaël

**16/12/2019**
* Ajout du fichier avec le code pour R-JSON pour le POC R-JSON

**09/12/2019**
* Ajout du fichier avec le code pour RO pour le POC RO v2

**02/12/2019**
* Ajout du fichier avec le code pour RO pour le POC RO v1

**25/11/2019**
* Ajout du fichier avec le code sous NEO4J
* Ajout un fichier.png pour le diagramme sous NEO4J
* Pour le POC NEO4J

**18/11/2019**
* Ajout du fichier MongoDB
* Pour le POC MongoDB

**21/10/2019**
* Modification NDC
* Modification du code UML mais aussi du diagramme
* Modification de la MLD
* Modification du code SQL avec multiple entrée

**14/10/2019**
* Légère modification du diagramme UML
* Légère Modification de la MLD
* Ajout du code SQL + incrémentation de la base

**07/10/2019**
* Modification UML https://gitlab.utc.fr/na17-ebay/na17-project/blob/master/projetUML.pdf
* et du code : https://gitlab.utc.fr/na17-ebay/na17-project/blob/master/Code-UML.md
* Ajout MLD  :https://gitlab.utc.fr/na17-ebay/na17-project/blob/master/mld%20

**30/09/2019** : 
*  Modification au niveau de la NDC : ajout de plusieurs éléments.
*  Ajout du diagramme UML.
*  Ajout du code du diagramme UML.